<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class EmailController extends MY_Controller {

    public function __construct() {
        parent:: __construct();

        $this->load->helper('url');
       // $this->load->model('Login_model');
    }

    /*public function index() {
        $this->load->view('email/contact');
    }*/

    public function send() {
        $json_obj = $this->readJson();
        $userId = isset($json_obj->userId) ? trim($json_obj->userId): '';	
        
       
		if( !$userId ){
			$this->jsonOutput(['status'=> 'fail', 'message' => 'userId is blank']);     
        }
        
       // echo("userid ".$userId);
            $this->db
            ->select('mobile_number as mobileNumber,name,business_name as businessName,location,is_pickup_available as isPickupAvailable,is_freelancer as isFreelancer')
            ->from('user')
            ->where('user_id',$userId)
            ->where('is_active', 1)
            ->where('is_deleted',0);
            $userData = $this->db->get()->row();
            //var_dump($userData);
            $mobileNumber = $userData->mobileNumber;
            $name = $userData->name;
            $businessName = $userData->businessName;
            $location = $userData->location;
            $isPickupAvailable = $userData->isPickupAvailable;
            $isFreelancer = $userData->isFreelancer;
                               
             
        
        $this->load->config('email');
        $this->load->library('email');
        
        $from = $this->config->item('smtp_user');
        $to = "iamharihk@gmail.com";

        $subject = "Outsource Project";
        $message = "test123";
        
        $this->email->initialize();
        $this->email->set_newline("\r\n"); 
        
        $this->email->from($from);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        
        if ($this->email->send()) {
            $result =  (object)['status' => true, 'message' => 'Your Email has successfully been sent.'];

            $this->jsonOutput($result);
            } 
            $result =  (object) ['status' => false, 'message' => 'something went wrong'];
            $this->jsonOutput($result);
}
}