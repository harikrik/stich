<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login_model extends CI_Model {


public function login($mobileNumber){
	$otp = $this->common->otpGenerator(4);
	$this->db
    		->select('user_id as userId,is_active as isActive')
    		->from('user')
			->where('mobile_number', $mobileNumber);
		$data = $this->db->get()->row();
			
		if(!$data){			
			
			$isnert_a = ['mobile_number' => $mobileNumber, 'otp' => $otp];
    		$this->db->insert('user', $isnert_a);
			$inserted_id = $this->db->insert_id();
			
			if($inserted_id){
				$this->sendOtp($mobileNumber,$otp);
				return ['status' => true, 'message' => 'otp generated successfully'];
			}	
			return ['status' => false, 'message' => 'something went wrong'];
		}	
		$userId = $data->userId;
		$isActive = $data->isActive;
        $update_a = ['otp' => $otp];
		$this->db
				->where('user_id',$userId )
				->update('user', $update_a);
		$update_res = $this->db->affected_rows();
    	if($update_res){
			$resp = $this->sendOtp($mobileNumber,$otp);
			return ['status' => true, 'message' => 'Otp Generated'];
			
    	}else{
    		return ['status' => false, 'message' => 'Sorry some error occured!'];
    	}			
  
    

}


public function validateOtp($mobileNumber,$otp){
	$this->db
    		->select('*')
    		->from('user')
    		->where('otp', $otp)
    		->where('mobile_number', $mobileNumber);

		$data = $this->db->get()->row();
		if(!$data){
			return (object)['status' => false, 'message' => 'Please enter a valid otp'];
		}        
		$userId = $data->user_id;
		$isActive = $data->is_active;
		$update_a = ['otp' => ''];
		$this->db
    		->where('user_id',$userId )
			->update('user', $update_a);
		$update_res = $this->db->affected_rows();
		if(!$isActive){
			//id, category_name, is_active, is_deleted
			$this->db
				->select('category_id as id,category_name as categoryName')
				->from('categories_m')
				->where('is_active', 1)
				->where('is_deleted',0);
			$categories  = $this->db->get()->result();	
			return (object)['status' => true, 'message' => 'Otp is valid',
			 'isActive' => $isActive, 'userId' => $userId,'categories' => $categories];
		}
    	return (object)['status' => true, 'message' => 'Otp is valid', 'userId' => $userId , 'isActive' => $isActive];
    	 	
    				
  
    

}

public function signup($name,$businessName,$id,$categories,$location,$isPickupAvailable,$isFreelancer){

    $this->db
        ->select('user_id as id,is_active as isActive')
        ->from('user')
		->where('user_id', $id);		
		$data = $this->db->get()->row();
		if(!$data){
			return (object)['status' => false, 'message' => "no user found"];
		 }	
		$update_a = ['name' => $name,'business_name' => $businessName,'is_active' => '1',
				'location' => $location,'is_pickup_available'=> $isPickupAvailable,'is_freelancer'=> $isFreelancer];
		$this->db->where('user_id', $id);
		$this->db->update('user', $update_a);
		$update_res = $this->db->affected_rows();
       
		for($i=0;$i<sizeof($categories);$i++){			
			$categoryId = $categories[$i]->id;
			$insert_a = ['user_id' => $id,'category_id' => $categoryId];
			$this->db->insert('user_categories', $insert_a);
    		$inserted_id = $this->db->insert_id();
		}
		return (object)['status' => true, 'message' => "user updated successfully",
		'data' => $data->id,'is_active'=> $data->isActive];
	
	
	}		

	public function addCustomer($mobileNumber,$name,$userId,$emailId){
		$this->db
			->select('*')
			->from('customer_vendor')
			->where('mobile_number', $mobileNumber)
			->where('user_id', $userId)
			->where('is_active', 1)
			->where('is_deleted',0);


		$data = $this->db->get()->row();
		if($data){
			return (object)['status' => true, 'message' => "user already exist"];
		 }
		
		 $insert_a = ['name' => $name,'email_id' => $emailId,'user_id' => $userId,'mobile_number'=> $mobileNumber];	
		 $this->db->insert('customer_vendor', $insert_a);
		 $inserted_id = $this->db->insert_id();
		 if($inserted_id){	
			return (object)['status' => true, 'message' => 'user updated successfully','custId' =>$inserted_id];

		 }
			return (object)['status' => false, 'message' => "something went wrong"];

		

		}	

		public function listCustomer($userId){
		$this->db
				->select('name as name,email_id as emailId,mobile_number as mobileNumber,cust_id as custId,is_active as isActive')
				->from('customer_vendor')
				->where('user_id',$userId)
				->where('is_active', 1)
				->where('is_deleted',0);
		$data = $this->db->get()->result();
		if(!$data){
			return (object)['status' => false, 'message' => "No Customer found"];
		 }
		 return (object)['status' =>true, 'data' => $data];

		}
		public function listItem($custId,$userId){
		
			
			//item_vendor	
			//item_v_id, user_id, item_id, tax, rate, created_at, updated_at, is_active, is_deleted	
			$this->db	
					->select('iv.item_id as itemId,im.item_name as itemName')					
					->from('item_vendor as iv')
					->join('item_master as im','im.item_id = iv.item_id')					
					->where('iv.user_id',$userId)
					->where('iv.is_active', 1)
					->where('iv.is_deleted',0);
			$data = $this->db->get()->result();
			if(!$data){
				return (object)['status' => false, 'message' => "no items found. contact admin to add items."];
			 }

			
			$this->db	
					->select('ci.item_id as itemId,im.item_name as itemName,ci.is_active as isActive')
					//->from('customer_item')
					->from('customer_item as ci')
					->join('item_master as im','im.item_id = ci.item_id')
					->where('ci.cust_id',$custId)
					->where('ci.user_id',$userId)
					->where('ci.is_active', 1)
					->where('ci.is_deleted',0);
			$dataX = $this->db->get()->result();		
			
			 for($i=0;$i<sizeof($dataX);$i++){			
				
				$this->db
				->select('cmd.meas_id as measId,cmd.measurements as measurement,m.measurement_name as measurementName,cmd.is_active as isActive')
				->from('customer_measurements_detail as cmd')
				->join('measurement as m','m.meas_id = cmd.meas_id')
				->where('cmd.cust_id', $custId)
				->where('cmd.user_id',$userId)
			    ->where('cmd.is_active', 1)
				->where('cmd.is_deleted',0);
			 $data_detail = $this->db->get()->result();
			 $dataX[$i]->measurements = $data_detail;
			 }
			 if(!$dataX){
				$dataX = [];
			 }			

			 return (object)['status' =>true, 'items' => $data,'measurements' =>  $dataX];

			}
public function listItemMaster($userId){
			
$this->db
	->select('im.item_name as itemName,im.item_id as itemId')
	->from('item_vendor as iv')
	->join('item_master as im','im.item_id = iv.item_id')
	->join('measurement as m','m.item_id = iv.item_id')
	->where('iv.user_id', $userId);

	 $items= $this->db->get()->row();
	 if(!$items){	
		return (object)['status' => false, 'message' => "something went Wrong"];
		}
		return (object)['status' => true, 'items' =>$items];
	}
		
private function getRange($minMeasurement,$maxMeasurement){
				$measurementRange = array();
				for($i=$minMeasurement;$i<=$maxMeasurement;$i++){
			  array_push($measurementRange,$i);
			 //print_r($a);
			}
				return	$measurementRange;
			}

            public function getItemMeasurement($itemId){
			$this->db 
			->select('meas_id as measId,measurement_name as measurementName,min_measurement as minMeasurement,max_measurement as maxMeasurement,is_active as isActive')
			->from('measurement')
			->where('item_id',$itemId)
			->where('is_active', 1)
			->where('is_deleted',0);
						
			$data = $this->db->get()->result();
			if(!$data){
				return (object)['status' => false, 'message' => "No item found"];
			 }
			 for($i=0;$i<sizeof($data);$i++){
				$minMeasurement = $data[$i]->minMeasurement;
				$maxMeasurement = $data[$i]->maxMeasurement;
				$measurementRange = $this->getRange(intval($minMeasurement),intval($maxMeasurement));
				$data[$i]->measurementRange = $measurementRange;
	
				 }
			 return (object)['status' =>true, 'data' => $data];
	
		}

		public function createItemVendor($userId,$measId,$aliasName,$itemId){
			
			date_default_timezone_set('Asia/Kolkata');
			$dateNow = date("Y-m-d H:i:s");
			$this->db
				->select('user_id as userId,is_active as isActive')
				->from('item_vendor')
				->where('item_id', $itemId)
				->where('user_id', $userId)
				->where('is_active', 1)
				->where('is_deleted',0);		
		$data = $this->db->get()->result();
		if(!$data){
		$insert_a = ['user_id' => $userId,'item_id'=> $itemId,'created_at' => $dateNow ];	
		$this->db->insert('item_vendor', $insert_a);
		$inserted_id = $this->db->insert_id();
				
		}
			
		 $this->db
				->select('user_id as userId,is_active as isActive')
				->from('item_vendor_detail')
				->where('meas_id', $measId)
				->where('user_id', $userId)
				->where('item_id', $itemId)
				->where('is_active', 1)
				->where('is_deleted',0);
		$data = $this->db->get()->result();
		if(!$data){
		 $insert_a = ['meas_id' => $measId,'alias_name' => $aliasName,'user_id' => $userId,'item_id' => $itemId,'created_at' => $dateNow ];	
		 $this->db->insert('item_vendor_detail', $insert_a);
		 $inserted_id = $this->db->insert_id();
		 if($inserted_id){	
			return (object)['status' => true, 'message' => 'updated successfully'];

		 }
			return (object)['status' => false, 'message' => "something went wrong"];

			
		}
		return (object)['status' => false, 'message' => "something went wrong"];
		
	}
		
		public function updateItemVendor($itemId,$tax,$rate){

        $this->db
        ->select('item_id as itemId,is_active as isActive')
        ->from('item_vendor')
		->where('item_id', $itemId);
			
		$data = $this->db->get()->row();
		if(!$data){
			return (object)['status' => false, 'message' => "no item found"];
		}
		$update_a = ['tax' => $tax,'rate' => $rate];
		$this->db->where('item_id', $itemId);
		$this->db->update('item_vendor', $update_a);
		$update_res = $this->db->affected_rows();
		if(!$update_res){	
			return (object)['status' => false, 'message' => "something went wrong"];

		 }
		 return (object)['status' => true, 'message' => 'updated successfully', 'data' => $data];
		}

		public function listItemVendor($userId){
			$this->db
					->select('item_id as itemId,item_v_id as itemVid,user_id as userId,tax as tax,rate as rate,created_at as createdAt,is_active as isActive')
					->from('item_vendor')
					->where('user_id', $userId)
				    ->where('is_active', 1)
					->where('is_deleted',0);
			$data = $this->db->get()->result();
			if(!$data){
				return (object)['status' => false, 'message' => "something went wrong"];
			 }
			 for($i=0;$i<sizeof($data);$i++){			
				$itemId = $data[$i]->itemId;
				$this->db
				->select('item_id as itemId,item_v_d_id as itemVDid,user_id as userId,meas_id as measId,alias_name as aliasName,created_at as createdAt,is_active as isActive')
				->from('item_vendor_detail')
				->where('item_id', $itemId)
				->where('user_id', $userId)
				->where('is_active', 1)
				->where('is_deleted',0);
			 $data_detail = $this->db->get()->result();
			 $data[$i]->itemVendorDetail = $data_detail;
			 
	
			}
			return (object)['status' => true,'data' => $data];
			


			}


		public function listWork(){
				$this->db
						->select('work_master_id as workMasterId,work as work')
						->from('work_master')
						->where('is_active', 1)
						->where('is_deleted',0);
				$data = $this->db->get()->result();
				if(!$data){
					return (object)['status' => false, 'message' => "something went wrong"];
				 }
				 return (object)['status' =>true, 'work' => $data];
		
				}

		public function createCustomerMeasurementOrderDetail($userId,$custId,$orderDetail){
			//($work,$userId,$measurements,$items,$custId,$deliveryDate,$voiceInstruction,$description,$status)
		date_default_timezone_set('Asia/Kolkata');
		$dateNow = date("Y-m-d H:i:s");

		$insert_a = ['cust_id' => $custId,'user_id' => $userId,'created_at' => $dateNow];	
		 $this->db->insert('order_master', $insert_a);
		 $inserted_id3 = $this->db->insert_id();

		for($i=0;$i<sizeof($orderDetail);$i++){
			
			$itemId = $orderDetail[$i]->itemId;
			
		 $insert_a = ['cust_id' => $custId,'user_id' => $userId,'item_id' => $itemId,'created_at' => $dateNow ];	
		 $this->db->insert('customer_item', $insert_a);
		 $inserted_id = $this->db->insert_id();
		 
		 $measurements = $orderDetail[$i]->measurements;
		 
		 $isNew = $orderDetail[$i]->isNew;
		 $isNew == 0;

		 if(!$isNew){
			
			for($j=0;$j<sizeof($measurements);$j++){
				$measId = $measurements[$j]->measId;
				$measurement = $measurements[$j]->measurement; 
				$itemId = $orderDetail[$i]->itemId;
				$insert_a = ['cust_id' => $custId,'user_id' => $userId,'item_id' => $itemId,'created_at' => $dateNow,'meas_id' => $measId,'measurements' => $measurement,'customer_item_id' =>$inserted_id  ];	
				$this->db->insert('customer_measurements_detail', $insert_a);
				$inserted_id2 = $this->db->insert_id();
	
			}
			 
			
}


		
		 /*$insert_a = ['cust_id' => $custId,'user_id' => $userId,'created_at' => $dateNow];	
		 $this->db->insert('order_master', $insert_a);
		 $inserted_id3 = $this->db->insert_id();*/

		 //for($i=0;$i<sizeof($orderDetail);$i++){
			$itemId = $orderDetail[$i]->itemId;
			$description = $orderDetail[$i]->description;
			//$voiceInstruction = $orderDetail[$i]->voiceInstruction;
			//$status = $orderDetail[$i]->status;
			$deliveryDate = $orderDetail[$i]->deliveryDate;
         $insert_a = ['order_id' => $inserted_id3,'delivery_date' => $deliveryDate,'item_id' => $itemId,'description' => $description,'created_at' => $dateNow,'customer_item_id' =>$inserted_id ];	
		 $this->db->insert('order_item_details_1', $insert_a);
		 $inserted_id4 = $this->db->insert_id();
		 //}
		 $work = $orderDetail[$i]->work;
		 for($k=0;$k<sizeof($work);$k++){
			$workMasterId = $work[$k]->workMasterId;
			$itemId = $orderDetail[$i]->itemId;
			$orderD1Id = $inserted_id4; 
			$insert_a = ['order_d1_id' => $orderD1Id,'order_id' => $inserted_id3,'work_master_id' =>$workMasterId,'item_id' => $itemId];	
			$this->db->insert('order_work_detail', $insert_a);
			$inserted_id5 = $this->db->insert_id();
			$work[$i]->orderD1Id = $inserted_id5;


		}
		

	}
		

		 if($inserted_id5){	
			 return (object)['status' => true, 'message' => 'updated successfully','orderId' => $inserted_id3,'workMasterId' =>$workMasterId,'work' =>$work];
		  }
		return (object)['status' => false, 'message' => "something went wrong"];
	}		
	
	

	

	public function listOrder($orderId){

		$this->db
	->select('order_id as orderId,user_id as userId,cust_id as custId,order_amount as orderAmount,order_status as orderStatus,(order_status+1) as newStatus,created_at as orderDate,is_active as isActive')
					
	->from('order_master')
	->where('order_id', $orderId);
							   
			$orderMaster = $this->db->get()->row();
			if(!$orderMaster){
				return (object)['status' => false, 'message' => "no order exist"];
			 }
			
			 $custId = $orderMaster->custId;
			 $this->db
			        ->select('name as name,mobile_number as mobileNumber')
					->from('customer_vendor')
					
					->where('cust_id', $custId)
				    ->where('is_active', 1)
					->where('is_deleted',0);
			$customerVendor = $this->db->get()->row();
			if(!$customerVendor){
				return (object)['status' => false, 'message' => "something went wrong1"];
			 }
			
			 $this->db
			 ->select('oid1.customer_item_id as customerItemId,oid1.order_d1_id as orderD1Id,oid1.delivery_date as deliveryDate,oid1.voice_instruction as voiceInstruction,oid1.description as description,oid1.created_at as createdAt,oid1.is_active as isActive,im.item_name as itemName,im.is_active as isActive')
			 
			 ->from('order_item_details_1 as oid1')
			 ->join('item_master as im','im.item_id = oid1.item_id')
			 ->where('oid1.order_id', $orderId)
			 ->where('oid1.is_active', 1)
			 ->where('oid1.is_deleted',0);
			 $orderItemDetails1 = $this->db->get()->row();
			 if(!$orderItemDetails1){
				return (object)['status' => false, 'message' => "something went wrong2"];
			 }
			 

			 //for($i=0;$i<sizeof($orderItemDetails1);$i++){
				$customerItemId = $orderItemDetails1->customerItemId; 			

				$this->db
					->select('cmd.cust_meas_id as custMeasId,cmd.cust_id as custId,cmd.user_id as userId,cmd.meas_id as measId,cmd.customer_item_id as customerItemId,cmd.item_id as itemId,cmd.measurements as measurements,cmd.is_active as  isActive,me.measurement_name,me.min_measurement as minMeasurement,me.max_measurement as maxMeasurement')
					
					->from('customer_measurements_detail as cmd')
					->join('customer_item as ci','ci.customer_item_id = cmd.customer_item_id')
					->join('measurement as me','me.meas_id = cmd.meas_id')					
					->where('ci.customer_item_id',$customerItemId);
				$custItemMeasurement = $this->db->get()->result();	
				//$orderItemDetails1->custItemMeasurement = $custItemMeasurement;
			 //}
			if(!$custItemMeasurement){
				return (object)['status' => false, 'message' => "something went wrong3"];
			 }
			 
			

			 $this->db
			 ->select('owd.order_work_detail_id as orderWorkDetailId,owd.work_master_id as workMasterId,owd.item_id as itemId,owd.is_active as isActive,wm.work as work') 
			 ->from('order_work_detail as owd')
			 ->join('work_master as wm','wm.work_master_id = owd.work_master_id')
			 ->where('owd.order_id', $orderId);
			  
	         $work = $this->db->get()->result();

			 return (object)['status' => true,'orderMaster' => $orderMaster,'customerVendor ' => $customerVendor ,'custItemMeasurement' => $custItemMeasurement,'item' =>$orderItemDetails1,'work' =>$work];
			}
	public function payment($orderId,$work,$itemId){
                $totalAmount = 0.00;
				for($i=0;$i<sizeof($work);$i++){
					$orderWorkDetailId = $work[$i]->orderWorkDetailId;
					$amount= $work[$i]->amount;
					

					
					
		      $update_a = ['amount' =>$amount];
			  $this->db->where('order_work_detail_id', $orderWorkDetailId);
		      $this->db->update('order_work_detail', $update_a);
			  $update_res = $this->db->affected_rows();
			  $totalAmount = $totalAmount+$amount;
				 
			  $update_a = ['is_active' => '1','order_amount' => $totalAmount];
			  $this->db->where('order_id', $orderId);
		      $this->db->update('order_master', $update_a);
		      $update_res = $this->db->affected_rows();
			
		}


				
			 
		    if(!$update_res){	
			return (object)['status' => false, 'message' => "something went wrong2"];

		   }
		   return (object)['status' => true, 'message' => 'updated successfully'];
		}


	
public function listPayment($orderId,$custId){

                $this->db
		            ->select('oid1.customer_item_id as customerItemId,oid1.order_d1_id as orderD1Id,oid1.delivery_date as deliveryDate,oid1.voice_instruction as voiceInstruction,oid1.description as description,oid1.created_at as createdAt,oid1.is_active as isActive,im.item_name as itemName,im.is_active as isActive')
					->from('order_item_details_1 as oid1')
					->join('item_master as im','im.item_id = oid1.item_id')
					->where('oid1.order_id', $orderId);
					 
			$orderWorkDetail = $this->db->get()->result();
			 
			for($i=0;$i<sizeof($orderWorkDetail);$i++){
				$orderD1Id = $orderWorkDetail[$i]->orderD1Id;
				$this->db
				->select('owd.order_work_detail_id as orderWorkDetailId,owd.work_master_id as workMasterId,owd.item_id as itemId,owd.is_active as isActive,owd.amount as amount,wm.work as work')					
				->from('order_work_detail as owd')
				->join('work_master as wm','wm.work_master_id = owd.work_master_id')
				->where('owd.order_d1_id',$orderD1Id);
				
    
	$data_detail = $this->db->get()->result();
	$orderWorkDetail[$i]->itemVendorDetail = $data_detail;
	
}
	$this->db
	->select('name as name,mobile_number as mobileNumber')
	->from('customer_vendor')
	->where('cust_id', $custId)
	->where('is_active', 1)
	->where('is_deleted',0);
$customerVendor = $this->db->get()->row();
if($customerVendor){
return (object)['status' => true,'orderWorkDetail' => $orderWorkDetail,'userDetails' => $customerVendor ];
}		       
			
}

public function getjobcount($userId){
date_default_timezone_set('Asia/Kolkata');
$dateNow = date("Y-m-d");
$the_day_of_week = date("w",strtotime($dateNow)); //sunday is 0
$first_day_of_week = date("Y-m-d",strtotime( $dateNow )-60*60*24*($the_day_of_week)+60*60*24*1 );
 $last_day_of_week = date("Y-m-d",strtotime($first_day_of_week)+60*60*24*6 );
 $first_day_of_comming_week = date("Y-m-d",strtotime( $dateNow )-60*60*24*($the_day_of_week)+60*60*24*8 );
 $last_day_of_comming_week = date("Y-m-d",strtotime($first_day_of_week)+60*60*24*13 );
 $first_day_of_month = date('Y-m-01', strtotime($dateNow));
 $last_day_of_month = date('Y-m-t', strtotime($dateNow));

$this->db
	->select('job_table_id as jobTableId ,job_status as jobStatus,is_active as isActive')
	->from('job_master')	
	->where('is_active', 1)
	->where('is_deleted',0);
	$jobs = $this->db->get()->result();
	
for($i=0;$i<sizeof($jobs);$i++){
	$jobTableId= $jobs[$i]->jobTableId;
		

if($jobTableId==1){//pending
	$this->db
	->select('count(order_id) as count')
	->from('order_master')
	->where('user_id', $userId)
    ->where('order_status',0);
    //->where('is_active', 1)
	//->where('is_deleted',0);
$pendingJobs= $this->db->get()->row();

$jobs[$i]->count = $pendingJobs->count;

}

elseif($jobTableId==2){//thisWeek
	
	$this->db
		->select('count(oid1.order_id) as count')
		->from('order_item_details_1 as oid1')
		->join('customer_item as ci','ci.customer_item_id = oid1.customer_item_id')
		->where('ci.user_id', $userId)
		->where('oid1.delivery_date <=', $last_day_of_week)
		->where('oid1.delivery_date >=', $dateNow)
		->where('oid1.delivery_date >=', $first_day_of_week);
		
	$thisWeek= $this->db->get()->row();
	$jobs[$i]->count = $thisWeek->count;
	
	//$jobs[$i]->count = 0;
	}
elseif($jobTableId==3){//comingWeek
	
	$this->db
	->select('count(oid1.order_id) as count')
	->from('order_item_details_1 as oid1')
	->join('customer_item as ci','ci.customer_item_id = oid1.customer_item_id')
	->where('ci.user_id', $userId)
	->where('oid1.delivery_date <=', $last_day_of_comming_week)
	->where('oid1.delivery_date >=', $dateNow)
	->where('oid1.delivery_date >=', $first_day_of_comming_week);
	
$comingWeek= $this->db->get()->row();
$jobs[$i]->count = $comingWeek->count;
//$jobs[$i]->count = 0;
}
elseif($jobTableId==4){//thisMonth
	
	$this->db
		->select('count(oid1.order_id) as count')
		->from('order_item_details_1 as oid1')
		->join('customer_item as ci','ci.customer_item_id = oid1.customer_item_id')
		->where('ci.user_id', $userId)
		->where('oid1.delivery_date <=', $last_day_of_month)
	    ->where('oid1.delivery_date >=', $dateNow)
	    ->where('oid1.delivery_date >=', $first_day_of_month);
	$thisMonth= $this->db->get()->row();
	$jobs[$i]->count = $thisMonth->count;

	//$jobs[$i]->count = 0;
}
elseif($jobTableId==5){//due
$this->db
		->select('count(oid1.order_id) as count')
		->from('order_item_details_1 as oid1')
		->join('customer_item as ci','ci.customer_item_id = oid1.customer_item_id')
		->where('ci.user_id', $userId)
		->where('oid1.delivery_date <', $dateNow);
		//->where('oid1.is_active', 1)
		//->where('oid1.is_deleted',0);
	$due= $this->db->get()->row();
	$jobs[$i]->count = $due->count;
	}
	

elseif($jobTableId==6){//finished
$this->db
	->select('count(order_id) as count')
	->from('order_master')
	->where('user_id', $userId)
	->where('order_status',1);
    //->where('is_active', 1)
	//->where('is_deleted',0);
	$finished= $this->db->get()->row();
$jobs[$i]->count = $finished->count;

}
elseif($jobTableId==7){//delivered
$this->db
	->select('count(order_id) as count')
	->from('order_master')
	->where('user_id', $userId)
    ->where('order_status',2);
    //->where('is_active', 1)
	//->where('is_deleted',0);
$delivered= $this->db->get()->row();
$jobs[$i]->count = $delivered->count;
}
}


$this->db
->select('user_id as userId,name as name,business_name as businessName,is_active as isActive')
->from('user')
->where('user_id', $userId);

$user = $this->db->get()->result();
for($i=0;$i<sizeof($user);$i++){
	$userId= $user[$i]->userId;
	//$weekstart = strtotime("next Monday") - 604800; this week
	$this->db
		->select('count(oid1.order_id) as thisWeek')
		->from('order_item_details_1 as oid1')
		->join('customer_item as ci','ci.customer_item_id = oid1.customer_item_id')
		->where('ci.user_id', $userId)
		->where('oid1.delivery_date <=', $last_day_of_week)
		->where('oid1.delivery_date >=', $dateNow)
		->where('oid1.delivery_date >=', $first_day_of_week);
	$thisWeek= $this->db->get()->row();
	
	$user[$i]->thisWeek = $thisWeek->thisWeek;

	//$lastDate = strtotime('next Month'); this month

	$this->db
		->select('count(oid1.order_id) as thisMonth')
		->from('order_item_details_1 as oid1')
		->join('customer_item as ci','ci.customer_item_id = oid1.customer_item_id')
		->where('ci.user_id', $userId)
		->where('oid1.delivery_date <=', $last_day_of_month)
	    ->where('oid1.delivery_date >=', $dateNow)
	    ->where('oid1.delivery_date >=', $first_day_of_month);
	$thisMonth= $this->db->get()->row();
	
	$user[$i]->thisMonth = $thisMonth->thisMonth;

	$this->db
		->select('count(oid1.order_id) as today')
		->from('order_item_details_1 as oid1')
		->join('customer_item as ci','ci.customer_item_id = oid1.customer_item_id')
		->where('ci.user_id', $userId)
		->where('oid1.delivery_date =', $dateNow);
		
	$today= $this->db->get()->row();
	
	$user[$i]->today = $today->today;
	



}




		
return (object)['status' => true,'jobs' => $jobs,'user' => $user];





}

public function listJobItems($userId,$jobTableId){
	date_default_timezone_set('Asia/Kolkata');
	$dateNow = date("Y-m-d");
	$the_day_of_week = date("w",strtotime($dateNow)); //sunday is 0
	$first_day_of_week = date("Y-m-d",strtotime( $dateNow )-60*60*24*($the_day_of_week)+60*60*24*1 );
	 $last_day_of_week = date("Y-m-d",strtotime($first_day_of_week)+60*60*24*6 );
	 $first_day_of_comming_week = date("Y-m-d",strtotime( $dateNow )-60*60*24*($the_day_of_week)+60*60*24*8 );
	 $last_day_of_comming_week = date("Y-m-d",strtotime($first_day_of_week)+60*60*24*13 );
	 $first_day_of_month = date('Y-m-01', strtotime($dateNow));
	 $last_day_of_month = date('Y-m-t', strtotime($dateNow));
	
	if($jobTableId == 1){//pending
		$this->db
			->select('om.order_id as orderId,om.cust_id as custId,om.is_active as isActive,cv.name as name')
			->from('order_master as om')
			->join('customer_vendor as cv','cv.cust_id = om.cust_id')
			->where('om.user_id', $userId)
			->where('om.order_status',0);
			//->where('om.is_active', 1)
			//->where('om.is_deleted',0)
	$orders= $this->db->get()->result();
	
	for($i=0;$i<sizeof($orders);$i++){
	$orderId = $orders[$i]->orderId;
	
	$this->db
		    ->select('oid1.delivery_date as deliveryDate,im.item_name as itemName')					
			->from('order_item_details_1 as oid1')
			->join('item_master as im','im.item_id = oid1.item_id')
		    //->join('customer_item as ci','ci.customer_item_id = oid1.customer_item_id')				
			->where('oid1.order_id',$orderId);
			$data1= $this->db->get()->row();
			$orders[$i]->orderDetail = $data1;
	}
	}
	if($jobTableId == 2){//this week
		//$weekstart = strtotime("next Monday") - 604800; 
     $this->db
		->select('oid1.delivery_date as deliveryDate,oid1.is_active as isActive,oid1.order_id as orderId,oid1.item_id as itemId,cv.cust_id as custId,cv.name as name')
		->from('order_item_details_1 as oid1')
		->join('customer_item as ci','ci.customer_item_id = oid1.customer_item_id')
		->join('customer_vendor as cv','cv.cust_id = ci.cust_id')
		->where('cv.user_id', $userId)
		->where('oid1.delivery_date <=', $last_day_of_week)
		->where('oid1.delivery_date >=', $dateNow)
		->where('oid1.delivery_date >=', $first_day_of_week);
		
		//->where('om.is_active', 1)
		//->where('om.is_deleted',0);
$orders= $this->db->get()->result();


for($i=0;$i<sizeof($orders);$i++){
	$orderId = $orders[$i]->orderId;
	
	$this->db
		    ->select('oid1.delivery_date as deliveryDate,im.item_name as itemName')					
			->from('order_item_details_1 as oid1')
			->join('item_master as im','im.item_id = oid1.item_id')
		    //->join('customer_item as ci','ci.customer_item_id = oid1.customer_item_id')				
			->where('oid1.order_id',$orderId);
			$data1= $this->db->get()->row();
			$orders[$i]->orderDetail = $data1;
	}
	}

	if($jobTableId == 3){//comming week
		//$weekend = strtotime("next Monday") - 1;
		$this->db
		->select('oid1.delivery_date as deliveryDate,oid1.is_active as isActive,oid1.order_id as orderId,oid1.item_id as itemId,cv.cust_id as custId,cv.name as name')
		->from('order_item_details_1 as oid1')
		->join('customer_item as ci','ci.customer_item_id = oid1.customer_item_id')
		->join('customer_vendor as cv','cv.cust_id = ci.cust_id')
		->where('cv.user_id', $userId)
		->where('oid1.delivery_date <=', $last_day_of_comming_week)
	    ->where('oid1.delivery_date >=', $dateNow)
	    ->where('oid1.delivery_date >=', $first_day_of_comming_week);
		//->where('om.is_active', 1)
		//->where('om.is_deleted',0);
$orders= $this->db->get()->result();


for($i=0;$i<sizeof($orders);$i++){
	$orderId = $orders[$i]->orderId;
	
	$this->db
		    ->select('oid1.delivery_date as deliveryDate,im.item_name as itemName')					
			->from('order_item_details_1 as oid1')
			->join('item_master as im','im.item_id = oid1.item_id')
		    //->join('customer_item as ci','ci.customer_item_id = oid1.customer_item_id')				
			->where('oid1.order_id',$orderId);
			$data1= $this->db->get()->row();
			$orders[$i]->orderDetail = $data1;
	}
	}

	if($jobTableId == 4){//this month
		//$lastDate = strtotime('next Month');

		$this->db
		->select('oid1.delivery_date as deliveryDate,oid1.is_active as isActive,oid1.order_id as orderId,oid1.item_id as itemId,cv.cust_id as custId,cv.name as name')
		->from('order_item_details_1 as oid1')
		->join('customer_item as ci','ci.customer_item_id = oid1.customer_item_id')
		->join('customer_vendor as cv','cv.cust_id = ci.cust_id')
		->where('cv.user_id', $userId)
		->where('oid1.delivery_date <=', $last_day_of_month)
	    ->where('oid1.delivery_date >=', $dateNow)
	    ->where('oid1.delivery_date >=', $first_day_of_month);
		//->where('om.is_active', 1)
		//->where('om.is_deleted',0);
$orders= $this->db->get()->result();


for($i=0;$i<sizeof($orders);$i++){
	$orderId = $orders[$i]->orderId;
	
	$this->db
		    ->select('oid1.delivery_date as deliveryDate,im.item_name as itemName')					
			->from('order_item_details_1 as oid1')
			->join('item_master as im','im.item_id = oid1.item_id')
		    //->join('customer_item as ci','ci.customer_item_id = oid1.customer_item_id')				
			->where('oid1.order_id',$orderId);
			$data1= $this->db->get()->row();
			$orders[$i]->orderDetail = $data1;
	}
	}
	if($jobTableId == 5){//due
		$this->db
		->select('oid1.delivery_date as deliveryDate,oid1.is_active as isActive,oid1.order_id as orderId,oid1.item_id as itemId,cv.cust_id as custId,cv.name as name')
		->from('order_item_details_1 as oid1')
		->join('customer_item as ci','ci.customer_item_id = oid1.customer_item_id')
		->join('customer_vendor as cv','cv.cust_id = ci.cust_id')
		->where('cv.user_id', $userId)
		->where('oid1.delivery_date <', $dateNow);
		//->where('om.is_active', 1)
		//->where('om.is_deleted',0);
$orders= $this->db->get()->result();

for($i=0;$i<sizeof($orders);$i++){
	$orderId = $orders[$i]->orderId;		

			$this->db
		    ->select('oid1.delivery_date as deliveryDate,im.item_name as itemName')					
			->from('order_item_details_1 as oid1')
			->join('item_master as im','im.item_id = oid1.item_id')
		    //->join('customer_item as ci','ci.customer_item_id = oid1.customer_item_id')				
			->where('oid1.order_id',$orderId);
			$data1= $this->db->get()->row();
			$orders[$i]->orderDetail = $data1;
			}

	}

	if($jobTableId == 6){//finished
		$this->db
		    ->select('om.order_id as orderId,om.cust_id as custId,om.is_active as isActive,cv.name as name')
			->from('order_master as om')
			->join('customer_vendor as cv','cv.cust_id = om.cust_id')
			->where('om.user_id', $userId)
			->where('om.order_status',1);
			//->where('om.is_active', 1)
			//->where('om.is_deleted',0)
	$orders= $this->db->get()->result();
	
	for($i=0;$i<sizeof($orders);$i++){
		$orderId = $orders[$i]->orderId;
		
		$this->db
				->select('oid1.delivery_date as deliveryDate,im.item_name as itemName')					
				->from('order_item_details_1 as oid1')
				->join('item_master as im','im.item_id = oid1.item_id')
				//->join('customer_item as ci','ci.customer_item_id = oid1.customer_item_id')				
				->where('oid1.order_id',$orderId);
				$data1= $this->db->get()->row();
				$orders[$i]->orderDetail = $data1;
		}
	
	}
	if($jobTableId == 7){//delivered
		$this->db
            ->select('om.order_id as orderId,om.cust_id as custId,om.is_active as isActive,cv.name as name')
			->from('order_master as om')
			->join('customer_vendor as cv','cv.cust_id = om.cust_id')
			->where('om.user_id', $userId)
			->where('om.order_status',2);
			//->where('om.is_active', 1)
			//->where('om.is_deleted',0)
	$orders= $this->db->get()->result();
	
	for($i=0;$i<sizeof($orders);$i++){
		$orderId = $orders[$i]->orderId;
		
		$this->db
				->select('oid1.delivery_date as deliveryDate,im.item_name as itemName')					
				->from('order_item_details_1 as oid1')
				->join('item_master as im','im.item_id = oid1.item_id')
				//->join('customer_item as ci','ci.customer_item_id = oid1.customer_item_id')				
				->where('oid1.order_id',$orderId);
				$data1= $this->db->get()->row();
				$orders[$i]->orderDetail = $data1;
		}
	}

	if(!$orders){
		return (object)['status' => false, 'message' => "No order found"];
	 }
	 return (object)['status' =>true, 'orderDetail' => $orders];

	
}



public function listJobDetails($orderId){
	
$this->db
		->select('oid1.order_d1_id as orderD1Id,om.order_status as orderStatus,(om.order_status+1) as newStatus,oid1.item_id as itemId,oid1.customer_item_id as customerItemId,oid1.delivery_date as deliveryDate,oid1.sample_dress as sampleDress,oid1.voice_instruction as voiceInstruction,oid1.description as description,oid1.created_at as createdAt,oid1.is_active as isActive,cv.name as name')
		
		->from('order_item_details_1 as oid1')
		->join('customer_item as ci','ci.customer_item_id = oid1.customer_item_id')
		->join('customer_vendor as cv','cv.cust_id = ci.cust_id')
		->join('order_master as om','om.order_id = oid1.order_id')

		//->where('cv.user_id', $userId)
		->where('oid1.order_id', $orderId);
		//->where('om.is_active', 1)
		//->where('om.is_deleted',0);
$orders= $this->db->get()->result();

for($i=0;$i<sizeof($orders);$i++){
	$itemId = $orders[$i]->itemId;
		
$this->db
->select('item_id as itemId,item_name as itemName')
->from('item_master')
->where('item_id', $itemId)
->where('is_active', 1)
->where('is_deleted',0);
$orderItemDetails1 = $this->db->get()->result();
$orders[$i]->orderItemDetails1 = $orderItemDetails1;
if(!$orders){
   return (object)['status' => false, 'message' => "something went wrong2"];
}
$customerItemId = $orders[$i]->customerItemId; 	

$this->db
	   ->select('cmd.measurements as measurements,me.measurement_name')					
	   ->from('customer_measurements_detail as cmd')
	   ->join('measurement as me','me.meas_id = cmd.meas_id')					
	   ->where('cmd.customer_item_id',$customerItemId);
   $custItemMeasurement = $this->db->get()->result();	
   $orders[$i]->custItemMeasurement = $custItemMeasurement;

	$orderD1Id = $orders[$i]->orderD1Id;
	$this->db
	->select('owd.amount as amount,wm.work as work')					
	->from('order_work_detail as owd')
	->join('work_master as wm','wm.work_master_id = owd.work_master_id')
	->where('owd.order_d1_id',$orderD1Id);
	$workdetail= $this->db->get()->result();
	$orders[$i]->workdetail = $workdetail;	
	
}
return (object)['status' => true,'order' => $orders];
}
 public function test($userId){
	
	date_default_timezone_set('Asia/Kolkata');
	$dateNow = date("Y-m-d");
	$the_day_of_week = date("w",strtotime($dateNow)); //sunday is 0
 $first_day_of_week = date("Y-m-d",strtotime( $dateNow )-60*60*24*($the_day_of_week)+60*60*24*1 );
 $last_day_of_week = date("Y-m-d",strtotime($first_day_of_week)+60*60*24*6 );
 $first_day_of_comming_week = date("Y-m-d",strtotime( $dateNow )-60*60*24*($the_day_of_week)+60*60*24*8 );
 $last_day_of_comming_week = date("Y-m-d",strtotime($first_day_of_week)+60*60*24*13 );
 
$first_day_of_month = date('Y-m-01', strtotime($dateNow));
$last_day_of_month = date('Y-m-t', strtotime($dateNow));


 echo($last_day_of_month);
	
	die();

 
 $this->db
	->select('job_table_id as jobTableId ,job_status as jobStatus,is_active as isActive')
	->from('job_master')	
	->where('is_active', 1)
	->where('is_deleted',0);
	$jobs = $this->db->get()->result();
	
for($i=0;$i<sizeof($jobs);$i++){
	$jobTableId= $jobs[$i]->jobTableId;
		

if($jobTableId==2){//thisWeek
	//$weekstart = strtotime("next Monday") - 604800;
	$this->db
		->select('count(oid1.order_id) as count')
		->from('order_item_details_1 as oid1')
		->join('customer_item as ci','ci.customer_item_id = oid1.customer_item_id')
		->where('ci.user_id', $userId)
		->where('oid1.delivery_date <', $last_day_of_week)
		->where('oid1.delivery_date >', $dateNow)
		->where('oid1.delivery_date >', $first_day_of_week);
		
	$thisWeek= $this->db->get()->row();
	$jobs[$i]->count = $thisWeek->count;
	
	//$jobs[$i]->count = 0;
	}
	
}
//echo($thisWeek);
	
	//die();


return (object)['status' =>$jobs ];


}


public function createGalleryFolder($userId,$folderName){
	date_default_timezone_set('Asia/Kolkata');
	$dateNow = date("Y-m-d");
	$insert_a = ['folder_name' => $folderName,'user_id' => $userId,'created_at' => $dateNow];	
	$this->db->insert('gallery_folder', $insert_a);
	$galleryFolder = $this->db->insert_id();
	
	return (object)['status' => true,'message' => 'updated successfully'];
}


public function listGalleryFolder($userId){
	$this->db
	->select('gallery_table_id as galleryTableId,folder_name as folderName ')
	->from('gallery_folder')
	->where('user_id', $userId)
	->where('is_active', 1)
	->where('is_deleted',0);
	$galleryFolder = $this->db->get()->result();
	return (object)['status' => true,'galleryFolder' => $galleryFolder];
}
public function uploadImage($galleryTableId,$productImage,$userId){
	
	$dateNow = date("Y-m-d H:i:s");	
	$timestamp = strtotime($dateNow);
	$rand = $this->common->otpGenerator(6);
	$projectPath = FCPATH;
	$path = "/var/www/html/stich/uploads/gallery";
	$path = $projectPath."/uploads/gallery/";
	$productImageName = $rand.$timestamp.".jpeg";
	$filename = $path.$productImageName;
	
	$this->common->base64ToImage($productImage, $filename);
			
	$file_url = $this->s3_upload->upload_file($filename);
	//echo("123");
	//echo($file_url);
	unlink($filename);
	//die();


$insert_a = ['gallery_table_id' => $galleryTableId,'product_image' => $file_url,'user_id' => $userId];	
	$this->db->insert('gallery_details', $insert_a);
	$galleryDetails = $this->db->insert_id();
	
	return (object)['status' => true,'message' => 'updated successfully'];
}


public function listGalleryDetails($galleryTableId){
	$this->db
	->select('product_image as productImage')
	->from('gallery_details')
	->where('gallery_table_id', $galleryTableId)
	->where('is_active', 1)
	->where('is_deleted',0);
	$galleryDetails = $this->db->get()->result();
	
	if(!$galleryDetails){
		return (object)['status' => false, 'message' => "No image found"];
	 }
	 return (object)['status' => true,'galleryDetails' => $galleryDetails]; 

}


public function orderFinished($orderId,$orderStatus){//finished and delivered


//$update_a = ['amount' =>$amount];
//$this->db->where('order_work_detail_id', $orderWorkDetailId);
//$this->db->update('order_work_detail', $update_a);
//$update_res = $this->db->affected_rows();
//$totalAmount = $totalAmount+$amount;
   
$update_a = ['order_status' => $orderStatus];
$this->db->where('order_id', $orderId);
$this->db->update('order_master', $update_a);
$update_res = $this->db->affected_rows();


  
if(!$update_res){	
return (object)['status' => false, 'message' => "something went wrong2"];

}
return (object)['status' => true, 'message' => 'updated successfully'];
}
public function orderCancelled($orderId){


	
	$update_a = ['order_status' => '3'];
	$this->db->where('order_id', $orderId);
	$this->db->update('order_master', $update_a);
	$update_res = $this->db->affected_rows();
	
	
	  
	if(!$update_res){	
	return (object)['status' => false, 'message' => "something went wrong2"];
	
	}
	return (object)['status' => true, 'message' => 'updated successfully'];
	}
	
	
public function Updateitem($userId,$itemId){
	$this->db
	->select('item_id as itemId,user_id as userId')
	->from('item_vendor')
	->where('item_id', $itemId)
	->where('user_id', $userId)
	->where('is_active', 1)
	->where('is_deleted',0);
	 $items= $this->db->get()->row();
	 if($items){	
		return (object)['status' => false, 'message' => "Item already exist"];
		}
	
	$insert_a = ['item_id' => $itemId,'user_id' => $userId];
	$this->db->where('user_id', $userId);
	$this->db->insert('item_vendor', $insert_a);
	$items = $this->db->insert_id();
	
	return (object)['status' => true,'message' => 'updated successfully'];
	}

public function listUpdateitem($userId){

	$this->db
		->select('im.item_id as itemId,iv.user_id as userId,im.item_name as itemName')
		->from('item_vendor as iv')
		->join('item_master as im','im.item_id = iv.item_id')
		->where('iv.user_id', $userId);
		
		 $updatedItems= $this->db->get()->result();

	$this->db
	->select('item_name as itemName,item_id as itemId')
	->from('item_master')
	->where('is_active', 1)
	->where('is_deleted',0);

	 $masterItems= $this->db->get()->result();
	 
			
		return (object)['status' => true,'updatedItems' => $updatedItems,'masterItems' =>$masterItems];
		}
	








	



			


		














private function sendOtp($mobile,$otp){
		
			$curl = curl_init();
			$url = "http://2factor.in/API/V1/3460c6c7-acf6-11e9-ade6-0200cd936042/SMS/".$mobile."/".$otp."/ch_prod";
			curl_setopt_array($curl, array(
			  CURLOPT_URL => $url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_POSTFIELDS => "",
			  CURLOPT_HTTPHEADER => array(
				"content-type: application/x-www-form-urlencoded"
			  ),
			));
		
			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);
		
			if ($err) {
			  return (object)['status' => false, 'data' => $err];
			} else {
			   return (object)['status' => true, 'data' => $response];
			}
}
		



}


/*{
	public function createCustomerMeasurementOrderDetail($work,$userId,$measurements,$itemId,$custId,$deliveryDate,$voiceInstruction,$description,$status){
			
		date_default_timezone_set('Asia/Kolkata');
		$dateNow = date("Y-m-d H:i:s");

		
		 $insert_a = ['cust_id' => $custId,'user_id' => $userId,'item_id' => $itemId,'created_at' => $dateNow ];	
		 $this->db->insert('customer_item', $insert_a);
		 $inserted_id = $this->db->insert_id();
		 
		for($i=0;$i<sizeof($measurements);$i++){
			$measId = $measurements[$i]->measId;
			$measurement = $measurements[$i]->measurement; 
			$insert_a = ['cust_id' => $custId,'user_id' => $userId,'item_id' => $itemId,'created_at' => $dateNow,'meas_id' => $measId,'measurements' => $measurement,'customer_item_id' =>$inserted_id  ];	
			$this->db->insert('customer_measurements_detail', $insert_a);
			$inserted_id2 = $this->db->insert_id();

		}
		 $insert_a = ['cust_id' => $custId,'user_id' => $userId,'created_at' => $dateNow];	
		 $this->db->insert('order_master', $insert_a);
		 $inserted_id3 = $this->db->insert_id();

         $insert_a = ['order_id' => $inserted_id3,'delivery_date' => $deliveryDate,'item_id' => $itemId,'voice_instruction' => $voiceInstruction,'description' => $description,'status' => $status,'created_at' => $dateNow,'customer_item_id' =>$inserted_id ];	
		 $this->db->insert('order_item_details_1', $insert_a);
		 $inserted_id4 = $this->db->insert_id();

		 for($i=0;$i<sizeof($work);$i++){
			$workMasterId = $work[$i]->workMasterId;
			$orderD1Id = $inserted_id4; 
			$insert_a = ['order_d1_id' => $orderD1Id,'order_id' => $inserted_id3,'work_master_id' =>$workMasterId,'item_id' => $itemId];	
			$this->db->insert('order_work_detail', $insert_a);
			$inserted_id5 = $this->db->insert_id();
			$work[$i]->orderD1Id = $inserted_id5;


		}
		


		

		 if($inserted_id5){	
			 return (object)['status' => true, 'message' => 'updated successfully','orderId' => $inserted_id3,'workMasterId' =>$workMasterId,'work' =>$work];
		  }
		return (object)['status' => false, 'message' => "something went wrong"];
	}		 

}*/

/*$weekstart = strtotime("next Monday") - 604800; 
 
echo "start of week is: ".date("D M j G:i:s T Y", $weekstart)."\n"; 

$weekend = strtotime("next Monday") - 1;
 
echo "start of week is: ".date("D M j G:i:s T Y", $weekend)."\n"; */