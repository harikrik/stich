<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['api/login']     = 'api/Login/login';
$route['api/validateOtp']     = 'api/Login/validateOtp';
$route['api/signup']     = 'api/Login/signup';
$route['api/addCustomer']     = 'api/Login/addCustomer';
$route['api/listCustomer']     = 'api/Login/listCustomer';
$route['api/listItem']     = 'api/Login/listItem';
$route['api/getItemMeasurement']     = 'api/Login/getItemMeasurement';
$route['api/createItemVendor']     = 'api/Login/createItemVendor';
$route['api/updateItemVendor']     = 'api/Login/updateItemVendor';
$route['api/listItemVendor']     = 'api/Login/listItemVendor';
$route['api/createCustomerMeasurementOrderDetail']     = 'api/Login/createCustomerMeasurementOrderDetail';
$route['api/listOrder']     = 'api/Login/listOrder';
$route['api/listWork']     = 'api/Login/listWork';
$route['api/payment']     = 'api/Login/payment';    
$route['api/listPayment']     = 'api/Login/listPayment';
$route['api/getjobcount']     = 'api/Login/getjobcount';
$route['api/listJobItems']     = 'api/Login/listJobItems';
$route['api/listJobDetails']     = 'api/Login/listJobDetails';
$route['api/test']     = 'api/Login/test';
$route['api/createGalleryFolder']     = 'api/Login/createGalleryFolder';
$route['api/listGalleryFolder']     = 'api/Login/listGalleryFolder';
$route['api/uploadImage']     = 'api/Login/uploadImage';
$route['api/listGalleryDetails']     = 'api/Login/listGalleryDetails';
$route['api/orderFinished']     = 'api/Login/orderFinished';
$route['api/orderCancelled']     = 'api/Login/orderCancelled';
$route['api/listItemMaster']     = 'api/Login/listItemMaster';
$route['api/Updateitem']     = 'api/Login/Updateitem';
$route['api/listUpdateitem']     = 'api/Login/listUpdateitem';
$route['api/EmailController'] = 'api/EmailController/send';


